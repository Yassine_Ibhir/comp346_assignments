import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class Monitor
 * To synchronize dining philosophers.
 *
 * @author Serguei A. Mokhov, mokhov@cs.concordia.ca
 */
public class Monitor
{
	/*
	 * ------------
	 * Data members
	 * ------------
	 */
	private int numOfPhilosophers;

	private enum STATE  {THINKING, TALKING, EATING};

	private STATE [] states;

	private boolean isAnyPhiTalking = false;



	/**
	 * Constructor
	 */
	public Monitor(int piNumberOfPhilosophers)
	{
		// TODO: set appropriate number of chopsticks based on the # of philosophers
		numOfPhilosophers = piNumberOfPhilosophers;

		states = new STATE[numOfPhilosophers];



		for (int i =0; i<numOfPhilosophers; i++){
			states[i] = STATE.THINKING;

		}
	}



	/*
	 * -------------------------------
	 * User-defined monitor procedures
	 * -------------------------------
	 */

	/**
	 * Grants request (returns) to eat when both chopsticks/forks are available.
	 * Else forces the philosopher to wait()
	 */
	public synchronized void pickUp(final int piTID)  {

		int philosopherIndex = piTID - 1;

		while (states[((philosopherIndex+numOfPhilosophers) -1) % numOfPhilosophers] == STATE.EATING ||
				states[(philosopherIndex + 1) % numOfPhilosophers] == STATE.EATING )
		{
            try {
                wait();
            } catch (InterruptedException e) {
				e.printStackTrace();
            }
        }

		states[philosopherIndex] = STATE.EATING;
		// ...
	}

	/**
	 * When a given philosopher's done eating, they put the chopstiks/forks down
	 * and let others know they are available.
	 */
	public synchronized void putDown(final int piTID)
	{
		int philosopherIndex = piTID - 1;
		states[philosopherIndex] = STATE.THINKING;
		notifyAll();
		// ...
	}

	/**
	 * Only one philopher at a time is allowed to philosophy
	 * (while she is not eating).
	 */
	public synchronized void requestTalk(final int piTID)
	{
		int philosopherIndex = piTID - 1;

		while(isAnyPhiTalking || states[philosopherIndex] == STATE.EATING){
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

		isAnyPhiTalking = true;

		states[philosopherIndex] = STATE.TALKING;
		// ...
	}

	/**
	 * When one philosopher is done talking stuff, others
	 * can feel free to start talking.
	 */
	public synchronized void endTalk(final int piTID)
	{
		int philosopherIndex = piTID - 1;

		states[philosopherIndex] = STATE.THINKING;

		isAnyPhiTalking = false;

		notifyAll();

		// ...
	}
}

// EOF


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Kerly Titus
 */
public class Driver {

    /** 
     * main class
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    	Network objNetwork = new Network( );            /* Activate the network */
        objNetwork.start();

        Client objClient1 = new Client("sending");          /* Start the sending client thread */

        Client objClient2 = new Client("receiving");        /* Start the receiving client thread */


        Server server1 = new Server("Server1");
        Server server2 = new Server("Server2");
        Server server3 = new Server("Server3");

        server1.start();
        server2.start();
        server3.start();

        objClient1.start();
        objClient2.start();

        try {
            objClient1.join();
            objClient2.join();

            server1.interrupt();
            server2.interrupt();
            server3.interrupt();
            server1.join();
            server2.join();
            server3.join();

            objNetwork.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }


    }
    
 }


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author Kerly Titus
 */
public class Driver {

    /** 
     * main class
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	Network objNetwork = new Network("network");
        objNetwork.start();
        Server server = new Server();
        Client clientSending = new Client("sending");
        Client clientReceiving = new Client("receiving");
        server.start();
        clientSending.start();
        clientReceiving.start();
    }

}
